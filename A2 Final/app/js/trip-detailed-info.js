const VIEWKEY = "view_key"; // local storage key to store all user session data
const ALLTRIPS = "all_trips_key"; // local storage key to store all user session data
const VIEWINDEX = "view_index"; // local storage key to store current session view object index in the all_trips array

/**
 * checkLSData function
 * Used to check if any data in LS exists at a specific key
 * @param {string} key LS Key to be used
 * @returns true or false representing if data exists at key in LS
 */
function checkLSData(key) {
  if (localStorage.getItem(key) != null) {
    return true;
  }
  return false;
}

/**
 * retrieveLSData function
 * Used to retrieve data from LS at a specific key.
 * @param {string} key LS Key to be used
 * @returns data from LS in JS format
 */
function retrieveLSData(key) {
  let data = localStorage.getItem(key);
  try {
    data = JSON.parse(data);
  } catch (err) {
  } finally {
    return data;
  }
}

/**
 * updateLSData function
 * Used to store JS data in LS at a specific key
 * @param {string} key LS key to be used
 * @param {any} data data to be stored
 */
function updateLSData(key, data) {
  let json = JSON.stringify(data);
  localStorage.setItem(key, json);
}

let retrievedView; // Current view object stored in local storage.
let points = []; // Marker coordinates.
let pointPairs = []; // Coordinate pairs for each polyline start and end positions.

// Run the necessary operations to render the markers and polylines by fetching the current view object from local storage.
if (checkLSData(VIEWKEY)) {
  retrievedView = retrieveLSData(VIEWKEY);
  generateMarkers();
  generatePoints();
  render(points, pointPairs);
  showSummary();
}

/**
 * generateMarkers function
 * Generates coordinates to render markers.
 */
function generateMarkers() {
  for (let i = 0; i < retrievedView.stack.length; i++) {
    let point = {
      lnglat: {
        lng: 0,
        lat: 0,
      },
      name: "",
      obj: null,
    };

    point.lnglat.lng = retrievedView.stack[i].source.longitude;
    point.lnglat.lat = retrievedView.stack[i].source.latitude;
    point.name = retrievedView.stack[i].source.name;
    point.obj = retrievedView.stack[i].source;

    points.push(point);
  }
}

/**
 * generatePoints function
 * Generates coordinate pairs to render polylines.
 */
function generatePoints() {
  for (let i = 0; i < retrievedView.stack.length - 1; i++) {
    let point = {
      lnglat: {
        lng: 0,
        lat: 0,
      },
      name: "",
      isSub: true,
    };

    let pointPair = [];

    let pointBase = retrievedView.stack[i].source;
    point.lnglat.lng = pointBase.longitude;
    point.lnglat.lat = pointBase.latitude;
    pointPair.push(point);

    point = {
      lnglat: {
        lng: 0,
        lat: 0,
      },
      name: "",
      isSub: true,
    };

    let nextPointBase = retrievedView.stack[i + 1].source;
    point.lnglat.lng = nextPointBase.longitude;
    point.lnglat.lat = nextPointBase.latitude;
    pointPair.push(point);

    pointPairs.push(pointPair);
  }
}

/**
 * render Function
 * Accepts data required to generate markers and polylines and render them on a map and show.
 * @param {[]} points Array of coordinates
 * @param {[]} pointPairs Array of coordinate pairs
 */

function render(points, pointPairs = []) {
  let map = renderMap();
  console.log(parseFloat(points[0].lnglat.lng));
  console.log(parseFloat(points[0].lnglat.lat));

  map.on("load", () => {
    for (let i = 0; i < points.length; i++) {
      const marker = new mapboxgl.Marker()
        .setLngLat(points[i].lnglat)
        .setPopup(
          // add popup
          new mapboxgl.Popup().setHTML(
            `<p style="margin:0; padding:0 !important;">${points[i].name}</p>`
          )
        )
        .addTo(map)
        .togglePopup();
    }

    for (let i = 0; i < pointPairs.length; i++) {
      let [point1, point2] = pointPairs[i];

      map.addSource(`route-${i}`, {
        type: "geojson",
        data: {
          type: "Feature",
          properties: {},
          geometry: {
            type: "LineString",
            coordinates: [
              [point1.lnglat.lng, point1.lnglat.lat],
              [point2.lnglat.lng, point2.lnglat.lat],
            ],
          },
        },
      });

      let routeColor = "";
      if (point1.isSub == true && point2.isSub == true) {
        routeColor = "#ff0000";
      } else {
        routeColor = "#000000";
      }

      map.addLayer({
        id: `route-${i}`,
        type: "line",
        source: `route-${i}`,
        layout: {
          "line-join": "round",
          "line-cap": "round",
        },
        paint: {
          "line-color": `${routeColor}`,
          "line-width": 2,
        },
      });
    }

    setTimeout(() => {
      map
        .setZoom(4)
        .panTo([
          parseFloat(points[0].lnglat.lng),
          parseFloat(points[0].lnglat.lat),
        ]);
    }, 500);
  });
}

/**
 * renderMap Function
 * Fetches a new map from mapbox api and returns it
 * @returns {Map} A mapboxgl map object
 */
function renderMap() {
  document.querySelector(".map-container").innerHTML =
    "<div id='map' style='width: 1200px; height: 600px;'></div>";

  mapboxgl.accessToken =
    "pk.eyJ1IjoibWFudXBhLXMiLCJhIjoiY2wyaHQxM2ZlMGgxYzNrbXhjYTh4bGt4NyJ9.E6lcnFeyr4FBuim4IKp6jw";

  let map = new mapboxgl.Map({
    container: "map",
    style: "mapbox://styles/mapbox/streets-v11",
  });

  map.panTo();

  map.on("click", function (e) {
    console.log(e.lngLat);
    let { lng, lat } = e.lngLat;
    map.panTo([lng, lat]);
  });

  return map;
}

let all_trips = [];
if (checkLSData(ALLTRIPS)) {
  all_trips = retrieveLSData(ALLTRIPS);
}

let index = 0;
if (checkLSData(VIEWINDEX)) {
  index = retrieveLSData(VIEWINDEX);
}

/**
 * showSummary Function
 * Uses the data in the current view object and displays the information
 */
function showSummary() {
  let journeyAirports = [];
  for (let i = 0; i < retrievedView.stack.length; i++) {
    journeyAirports.push(retrievedView.stack[i].source.name);
  }

  let journeySpanText = "";
  for (let i = 0; i < journeyAirports.length; i++) {
    if (journeyAirports.length == i + 1) {
      journeySpanText += journeyAirports[i];
    } else {
      journeySpanText += journeyAirports[i] + "➡";
    }
  }

  let journeyStops = `${journeyAirports.length - 2}`;

  document.querySelector(".journey-span").innerHTML = " " + journeySpanText;
  document.querySelector(".journey-stops").innerHTML = " " + journeyStops;
  document.querySelector(".journey-date").innerHTML = " " + retrievedView.date;
  document.querySelector(".journey-time").innerHTML = " " + retrievedView.time;

  let scheduledDate = new Date(`${retrievedView.date}T${retrievedView.time}`);
  let currentDate = new Date();

  if (currentDate.getTime() < scheduledDate.getTime()) {
    document.querySelector(".button-container").innerHTML = `<button
    type="button"
    class="cancel-btn mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent"
  >
    Cancel Trip
  </button>`;

    document.querySelector(".cancel-btn").addEventListener("click", () => {
      all_trips.splice(index, 1);
      updateLSData(ALLTRIPS, all_trips);
      window.location = "my-trips.html";
    });
  }


  document.querySelector(".button-container").insertAdjacentHTML('beforeend', `<button
    type="button"
    style="margin-left: 5px;"
    class="exit-btn mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent"
  >
    Save and Exit
  </button>`);

  document.querySelector(".exit-btn").addEventListener("click", () => {
    window.location = "index-mdl.html";
  });
}
