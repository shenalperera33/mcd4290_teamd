const ALLTRIPS = "all_trips_key"; // local storage key to store all user session data
const VIEWKEY = 'view_key'; // local storage key to store view object data for current session
const VIEWINDEX = 'view_index'; // local storage key to store current session view object index in the all_trips array

/**
 * checkLSData function
 * Used to check if any data in LS exists at a specific key
 * @param {string} key LS Key to be used
 * @returns true or false representing if data exists at key in LS
 */
function checkLSData(key) {
  if (localStorage.getItem(key) != null) {
    return true;
  }
  return false;
}

/**
 * retrieveLSData function
 * Used to retrieve data from LS at a specific key.
 * @param {string} key LS Key to be used
 * @returns data from LS in JS format
 */
function retrieveLSData(key) {
  let data = localStorage.getItem(key);
  try {
    data = JSON.parse(data);
  } catch (err) {
  } finally {
    return data;
  }
}

/**
 * updateLSData function
 * Used to store JS data in LS at a specific key
 * @param {string} key LS key to be used
 * @param {any} data data to be stored
 */
function updateLSData(key, data) {
  let json = JSON.stringify(data);
  localStorage.setItem(key, json);
}


let all_trips = [];
if (checkLSData(ALLTRIPS)) {
  all_trips = retrieveLSData(ALLTRIPS);
}


let completed_cards = "";
let upcoming_cards = "";

for (let i = 0; i < all_trips.length; i++) {
  const view = all_trips[i];

  let journeyAirports = [];
  for (let i = 0; i < view.stack.length; i++) {
    journeyAirports.push(view.stack[i].source.name);
  }
// generate the inner HTML text to display the starting points, stops and destinations
  let journeySpanText = "";
  for (let i = 0; i < journeyAirports.length; i++) {
    if (journeyAirports.length == i + 1) {
      journeySpanText += journeyAirports[i];
    } else {
      journeySpanText += journeyAirports[i] + "➡";
    }
  }

  let journeyStops = journeyAirports.length - 2;

  let scheduledDate = new Date(`${view.date}T${view.time}`);
  let currentDate = new Date();

  if (currentDate.getTime() < scheduledDate.getTime()) {
    console.log("upcoming");

    let upcoming_card = 
    `<div class="demo-card-wide mdl-card mdl-shadow--2dp trip-card">
        <div class="mdl-card__title">
          <h2 class="mdl-card__title-text">${journeyAirports[0]}➡${journeyAirports.at(-1)}</h2>
        </div>
        <div class="mdl-card__supporting-text">                    
              <ul class="demo-list-item mdl-list">
              <li class="mdl-list__item">
                <span class="mdl-list__item-primary-content">
                ${journeySpanText}
                </span>
              </li>
              <li class="mdl-list__item">
                <span class="mdl-list__item-primary-content">
                ${journeyStops}
                </span>
              </li>
              <li class="mdl-list__item">
                <span class="mdl-list__item-primary-content">
                  ${scheduledDate.toLocaleDateString()}
                </span>
              </li>
              <li class="mdl-list__item">
                <span class="mdl-list__item-primary-content">
                  ${scheduledDate.toLocaleTimeString()}
                </span>
              </li>
            </ul> 
        </div>
        <div class="mdl-card__actions mdl-card--border">
          <a
            class="mdl-button mdl-button--colored mdl-js-button mdl-js-ripple-effect"
            onclick="view(${i})"
          >
            View Detailed Info
          </a>
        </div>
    </div>`;
    upcoming_cards += upcoming_card;
    
  } else {

    let completed_card = 
    `<div class="demo-card-wide mdl-card mdl-shadow--2dp trip-card">
        <div class="mdl-card__title">
          <h2 class="mdl-card__title-text">${journeyAirports[0]}➡${journeyAirports.at(-1)}</h2>
        </div>
        <div class="mdl-card__supporting-text">                    
              <ul class="demo-list-item mdl-list">
              <li class="mdl-list__item">
                <span class="mdl-list__item-primary-content">
                ${journeySpanText}
                </span>
              </li>
              <li class="mdl-list__item">
                <span class="mdl-list__item-primary-content">
                ${journeyStops}
                </span>
              </li>
              <li class="mdl-list__item">
                <span class="mdl-list__item-primary-content">
                  ${scheduledDate.toLocaleDateString()}
                </span>
              </li>
              <li class="mdl-list__item">
                <span class="mdl-list__item-primary-content">
                  ${scheduledDate.toLocaleTimeString()}
                </span>
              </li>
            </ul> 
        </div>
        <div class="mdl-card__actions mdl-card--border">
          <a
            class="mdl-button mdl-button--colored mdl-js-button mdl-js-ripple-effect"
            onclick="view(${i})"
          >
            View Detailed Info
          </a>
        </div>
    </div>`;

    completed_cards += completed_card;
  }
}

document.querySelector(".upcoming-trips").innerHTML = upcoming_cards;
document.querySelector(".completed-trips").innerHTML = completed_cards;

/**
 * @param {int} index index of current trip
 */
function view(index) {
    
    console.log(all_trips[index]);
    updateLSData(VIEWINDEX, index);
    updateLSData(VIEWKEY, all_trips[index]);
    window.location = 'trip-detailed-info.html';
}