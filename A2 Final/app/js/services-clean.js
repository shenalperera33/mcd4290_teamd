const VIEWKEY = 'view_key';  // local storage key to store view object data for current session
const ALLTRIPS = 'all_trips_key'; // local storage key to store all user session data
let all_trips = [];

/**
 * webServiceRequest function
 * Generic web service request function to call a web service that supports jsonp.
 * @param {string} url address of the web service
 * @param {object} data object containing querystring params as key:value. must contain callback/jsonp attribute for jsonp
 */
function webServiceRequest(url, data) {
  // Build URL parameters from data object.
  let params = "";
  // For each key in data object...
  for (let key in data) {
    if (data.hasOwnProperty(key)) {
      if (params.length == 0) {
        // First parameter starts with '?'
        params += "?";
      } else {
        // Subsequent parameter separated by '&'
        params += "&";
      }

      let encodedKey = encodeURIComponent(key);
      let encodedValue = encodeURIComponent(data[key]);

      params += encodedKey + "=" + encodedValue;
    }
  }
  let script = document.createElement("script");
  script.src = url + params;
  document.body.appendChild(script);
}

/**
 * sendXMLRequestForPlaces function
 * This is a wrapper to call the MapBox Search API for points of interest
 * @param {string} query category of search. The following categories are supported: attraction, lodging, food, gas station
 * @param {number} lng the longitude coordinates
 * @param {number} lat the latitude coordinates
 * @param {function} successCallback contains the name of the function (not as a string) to call as the callback function when this web service request resolves
 */
function sendXMLRequestForPlaces(query, lng, lat, successCallback) {
  let url =
    "https://api.mapbox.com/geocoding/v5/mapbox.places/" +
    encodeURIComponent(query) +
    ".json?limit=10&proximity=" +
    lng +
    "," +
    lat +
    "&access_token=" +
    MAPBOX_KEY;
  let req = new XMLHttpRequest();
  req.open("GET", url, true);
  req.onload = function () {
    successCallback(JSON.parse(req.response));
  };
  req.send();
}

/**
 * sendXMLRequestForRoute function
 * This is a wrapper to call the MapBox Direction API for driving routing directions.
 * @param {number} startLat the start point latitude coordinates
 * @param {number} startLng the start point longitude coordinates
 * @param {number} endLat the end point latitude coordinates
 * @param {number} endLng the end point longitude coordinates
 * @param {function} directionsCallback contains the name of the function (not as a string) to call as the callback function when this web services request resolves
 */
function sendXMLRequestForRoute(
  startLat,
  startLon,
  endLat,
  endLon,
  directionsCallback
) {
  let url =
    "https://api.mapbox.com/directions/v5/mapbox/driving/" +
    startLon +
    "," +
    startLat +
    ";" +
    endLon +
    "," +
    endLat +
    "?steps=true&geometries=geojson&access_token=" +
    MAPBOX_KEY;
  let req = new XMLHttpRequest();
  req.open("GET", url, true);
  req.onload = function () {
    directionsCallback(JSON.parse(req.response));
  };
  req.send();
}

/**
 * sendWebServiceRequestForReverseGeocoding function
 * This is a wrapper to call the OpenCage Reverse Geocoder using the webServiceRequest function to obtain addresses from lat/lng coordinates
 * @param {number} lat the latitude coordinates
 * @param {number} lng the longitude coordinates
 * @param {string} callback contains the function name as a string to call as the callback function when this web services request resolves
 */
function sendWebServiceRequestForReverseGeocoding(lat, lng, callback) {
  let url = "https://api.opencagedata.com/geocode/v1/json";
  let data = {
    q: `${lat}+${lng}`,
    key: OPENCAGE_KEY,
    jsonp: callback,
  };
  webServiceRequest(url, data);
}

/**
 * sendWebServiceRequestForForwardGeocoding function
 * This is a wrapper to call the OpenCage Forward Geocoder using the webServiceRequest function to obtain lat/lng coordinates from an address
 * @param {string} location the address
 * @param {string} callback contains the function name as a string to call as the callback function when this web services request resolves
 */
function sendWebServiceRequestForForwardGeocoding(location, callback) {
  let url = "https://api.opencagedata.com/geocode/v1/json";
  let data = {
    q: `${location}`,
    key: OPENCAGE_KEY,
    jsonp: callback,
  };
  webServiceRequest(url, data);
}

/**
 * getUserCurrentLocationUsingGeolocation function
 * This is a wrapper to call the browser Geolocation API to request for the user's current location
 * @param {function} callback contains the name of the function (not as a string) to call as the callback function when this geolocation request resolves
 */
function getUserCurrentLocationUsingGeolocation(callback) {
  if ("geolocation" in navigator) {
    // geolocation is available
    navigator.geolocation.getCurrentPosition((position) => {
      callback(position.coords.latitude, position.coords.longitude);
    });
  } else {
    // geolocation IS NOT available
    console.log("Geolocation is not available");
  }
}

/**
 * checkLSData function
 * Used to check if any data in LS exists at a specific key
 * @param {string} key LS Key to be used
 * @returns true or false representing if data exists at key in LS
 */
function checkLSData(key) {
  if (localStorage.getItem(key) != null) {
    return true;
  }
  return false;
}

/**
 * retrieveLSData function
 * Used to retrieve data from LS at a specific key.
 * @param {string} key LS Key to be used
 * @returns data from LS in JS format
 */
function retrieveLSData(key) {
  let data = localStorage.getItem(key);
  try {
    data = JSON.parse(data);
  } catch (err) {
  } finally {
    return data;
  }
}

/**
 * updateLSData function
 * Used to store JS data in LS at a specific key
 * @param {string} key LS key to be used
 * @param {any} data data to be stored
 */
function updateLSData(key, data) {
  let json = JSON.stringify(data);
  localStorage.setItem(key, json);
}

/**
 * used to get the airports of the selected country
 * @param {String} country pass the selected country from country field
 * @param {String} callback the name of the function that needs to be called 
 */
function sendWebServiceRequestForAirports(country, callback) {
  let url = "https://eng1003.monash/OpenFlights/airports/";
  let airportPara = {
    country: country,
    callback: callback,
  };
  webServiceRequest(url, airportPara);
}

/**
 * get the routes from the seelcted airport
 * @param {string} sourceAirport selected airport
 * @param {string} callback function name to get called by th3 server
 */
function sendWebServiceRequestForRoutes(sourceAirport, callback) {
  let url = "https://eng1003.monash/OpenFlights/routes/";
  let routePara = {
    sourceAirport: sourceAirport,
    callback: callback,
  };
  webServiceRequest(url, routePara);
}

/*--- DOM Elements ---*/

let confirmBtn1 = document.querySelector(".confirm-btn-1");
let confirmBtn2 = document.querySelector(".confirm-btn-2");

let mainContainer = document.querySelector(".main-container");
let mapContainer = document.querySelector(".map-container");

let inputs1 = document.querySelector(".inputs-1");
let inputs2 = document.querySelector(".inputs-2");

let airportSelect = document.getElementById("airport-select");

document.querySelector(".undo-btn").style.display = "none";
document.querySelector(".trip-confirm-btn").style.display = "none";

let summary_container = document.querySelector(".summary-container");
summary_container.style.display = "none";

document.querySelector(".trip-save-btn").style.display = "none";
document.querySelector(".trip-cancel-btn").style.display = "none";


/*--- Globals ---*/
// GLobal object that contains data that's accessed globally and some helper functions 
let globalData = {
  initialData: null,
  domesticAirports: [],
  domesticAirportNames: [],
  domesticAirportIDs: [],

  domesticAirportsToName() {
    for (let i = 0; i < this.domesticAirports.length; i++) {
      this.domesticAirportNames.push(this.domesticAirports[i].name);
    }
  },

  domesticAirportsToID() {
    for (let i = 0; i < this.domesticAirports.length; i++) {
      this.domesticAirportIDs.push(this.domesticAirports[i].airportId);
    }
  },

  renderAirportSelect() {
    let option_set = `<option value="">--</option>`;
    for (let i = 0; i < this.domesticAirportNames.length; i++) {
      let option_element = `<option value="${this.domesticAirportNames[i]}">${this.domesticAirportNames[i]}</option>`;
      option_set += option_element;
    }
    airportSelect.innerHTML = option_set;
  },
};


// Trip class that defines a given airport and the associated unique domestic airports having a route.
class Trip {
  constructor(source, destinations) {
    this.source = source;
    this.destinations = destinations;
  }
}

// View class defines the data and behaviours required to present the map. 
class View {
  constructor() {
    this.stack = [];
    this.currentDestinations = [];
    this.date = "";
    this.time = "";
  }
  /**
   * transformRouteDestination
   * used to push all the routes from current destination to an array
   * @param {object} route route object that contains data about a destination airport
   */
  transeformRouteToDestination(route) {
    for (let i = 0; i < globalData.domesticAirports.length; i++) {
      if (
        route.destinationAirportId + "" ==
        globalData.domesticAirports[i].airportId
      ) {
        this.currentDestinations.push(globalData.domesticAirports[i]);
      }
    }
  }

/**
 * transformRoutesToDestination
 * loop through the all international and domestic routes 
 * @param {[]} routes all the international and domestic routes containing array
 */
  transeformRoutesToDestinations(routes) {
    console.log(routes);
    for (let i = 0; i < routes.length; i++) {
      this.transeformRouteToDestination(routes[i]);
    }

    this.filterDestinations();
  }
/**
 * getDestinations method
 * sends a get request to fetch outgoing route objects forom a given airports
 * @param {{}} source An airport object
 */
  getDestinations(source) {
    sendWebServiceRequestForRoutes(source.airportId, "getRoutes");
  }

/**
 * filterDestinations method
 * filters the current detination airports which are domestic and unique.  
 * @returns {[]} uniqueDestinations Array of airport objects  
 */
  filterDestinations() {
    let uniqueDestinations = this.currentDestinations.filter((c, index) => {
      return this.currentDestinations.indexOf(c) === index;
    });
    this.currentDestinations = uniqueDestinations;
    return uniqueDestinations;
  }
  
/**
 * addTrip function
 * call getDestinations method and create a new trip object, push it to stack containing trips and call generatePoints method
 * @param {{}} source An airport object
 */
  addTrip(source) {
    this.getDestinations(source);
    setTimeout(() => {
      let destinations = this.currentDestinations;
      let trip = new Trip(source, destinations);
      this.stack.push(trip);
      this.generatePoints();
    }, 1000);
  }
  /**
   * undo function
   * once user clicked undo, pop the trip object away from the stack
   */
  undo(){

    this.stack.pop();
    this.generatePoints();
  }
/**
 * renderSummary function
 * render a summary of the trip including starting point, stops and destination. Display the complete pathway of the user in the map
 */
  renderSummary(){

    // let summary_container = "<div class='summary-container'>";
    document.querySelector(".map-container").style.display = "none";
    document.querySelector(".undo-btn").style.display = "none";
    document.querySelector(".trip-confirm-btn").style.display = "none";
    summary_container.style.display = "block";

    document.querySelector(".trip-save-btn").style.display = "block ";
    document.querySelector(".trip-cancel-btn").style.display = "block";    


    let journeyAirports = [];
    for (let i = 0; i < this.stack.length; i++) {
      journeyAirports.push(this.stack[i].source.name);
    }

    let journeySpanText = '';
    for (let i = 0; i < journeyAirports.length; i++) {
      if (journeyAirports.length == i +1){
        journeySpanText += journeyAirports[i];
      }
      else{

        journeySpanText += journeyAirports[i] + '➡';
      } 

    }
    document.querySelector(".journey-span").innerHTML = " " + journeySpanText;
     

    console.log(journeyAirports);
    let journeyStops = `${journeyAirports.length-2}`;
    console.log(journeyStops);
    document.querySelector(".journey-stops").innerHTML = " " + journeyStops;

    let date = globalData.initialData.selectedDate;
    document.querySelector(".journey-date").innerHTML = " " + date;


    let time = globalData.initialData.selectedTime;
    document.querySelector(".journey-time").innerHTML = " " + time;
  }

  /**
 * generatePoints function
 * Generates coordinates to render markers.
 * Generates coordinate pairs to render polylines.
 * And calls the render method to update the view.
 */
  generatePoints() {
      let points = [];
      let pointPairs = [];

    // -----------------------------get coords of ports-----------------
    for (let i = 0; i < this.stack.length; i++) {
      let point = {
        lnglat: {
          lng: 0,
          lat: 0,
        },
        name: "",
        obj: null,
      };

      point.lnglat.lng = this.stack[i].source.longitude;
      point.lnglat.lat = this.stack[i].source.latitude;
      point.name = this.stack[i].source.name;
      point.obj = this.stack[i].source;

      points.push(point);
    }
    // get coods of peek routes

    for (let i = 0; i < this.stack.at(-1).destinations.length; i++) {
      let destinationPeekArr = this.stack.at(-1).destinations;

      let point = {
        lnglat: {
          lng: 0,
          lat: 0,
        },
        name: "",
        obj: null,
      };

      point.lnglat.lng = destinationPeekArr[i].longitude;
      point.lnglat.lat = destinationPeekArr[i].latitude;
      point.name = destinationPeekArr[i].name;
      point.obj = destinationPeekArr[i];
      points.push(point);
    }
    // ---------------------------Generate pointpair---------------------------

    // generate the routePoints

    for (let i = 0; i < this.stack.at(-1).destinations.length; i++) {
      let point = {
        lnglat: {
          lng: 0,
          lat: 0,
        },
        name: "",
        isSub: false,
      };
      let pointPair = [];

      let sourceAirport = this.stack.at(-1).source;
      point.lnglat.lng = sourceAirport.longitude;
      point.lnglat.lat = sourceAirport.latitude;
      pointPair.push(point);

      point = {
        lnglat: {
          lng: 0,
          lat: 0,
        },
        name: "",
        isSub: false,
      };

      point.lnglat.lng = this.stack.at(-1).destinations[i].longitude;
      point.lnglat.lat = this.stack.at(-1).destinations[i].latitude;
      pointPair.push(point);

      pointPairs.push(pointPair);
    }
    // render the base points

    for (let i = 0; i < this.stack.length - 1; i++) {
      let point = {
        lnglat: {
          lng: 0,
          lat: 0,
        },
        name: "",
        isSub: true,
      };
      let pointPair = [];

      let pointBase = this.stack[i].source;
      point.lnglat.lng = pointBase.longitude;
      point.lnglat.lat = pointBase.latitude;
      pointPair.push(point);

      point = {
        lnglat: {
          lng: 0,
          lat: 0,
        },
        name: "",
        isSub: true,
      };

      let nextPointBase = this.stack[i + 1].source;
      point.lnglat.lng = nextPointBase.longitude;
      point.lnglat.lat = nextPointBase.latitude;
      pointPair.push(point);

      pointPairs.push(pointPair);
    }

    console.log(points);
    console.log(pointPairs);
    this.render(points, pointPairs);
  }

/**
 * render method
 * Accepts data required to generate markers and polylines and render them on a map and show.
 * @param {[]} points Array of coordinates
 * @param {[]} pointPairs Array of coordinate pairs
 */

  render(points, pointPairs = []) {
    let map = renderMap();
    console.log(parseFloat(points[0].lnglat.lng));
    console.log(parseFloat(points[0].lnglat.lat));
    
    map.on("load", () => {
    
      for (let i = 0; i < points.length; i++) {
        const marker = new mapboxgl.Marker()
          .setLngLat(points[i].lnglat)
          .setPopup(
            // add popup
            new mapboxgl.Popup().setHTML(
              `<p style="margin:0; padding:0 !important;">${points[i].name}</p>`
            )
          )
          .addTo(map)
          .togglePopup()
          .getElement()
          .addEventListener("click", (e) => {
            console.log(points[i].obj);
            console.log(e);
            this.addTrip(points[i].obj);
          });
      }

      for (let i = 0; i < pointPairs.length; i++) {
        let [point1, point2] = pointPairs[i];

        map.addSource(`route-${i}`, {
          type: "geojson",
          data: {
            type: "Feature",
            properties: {},
            geometry: {
              type: "LineString",
              coordinates: [
                [point1.lnglat.lng, point1.lnglat.lat],
                [point2.lnglat.lng, point2.lnglat.lat],
              ],
            },
          },
        });

        let routeColor = "";
        if (point1.isSub == true && point2.isSub == true) {
          routeColor = "#ff0000";
        } else {
          routeColor = "#000000";
        }

        map.addLayer({
          id: `route-${i}`,
          type: "line",
          source: `route-${i}`,
          layout: {
            "line-join": "round",
            "line-cap": "round",
          },
          paint: {
            "line-color": `${routeColor}`,
            "line-width": 2,
          },
        });
      }
      
      setTimeout(() => {
        map.setZoom(4).panTo([parseFloat(points[0].lnglat.lng), parseFloat(points[0].lnglat.lat)]); 
      }, 500);
      document.querySelector(".undo-btn").style.display = "block";
      document.querySelector(".trip-confirm-btn").style.display = "block";
    });
  }
}

let view = new View();

/*--- Initial view ---*/

inputs2.style.display = "none";
mapContainer.style.display = "none";

/**
 * fetchInitialData Function
 * Fetches the user inputs for country, date and time.
 * @returns {{}} initialData Object
 */
function fetchInitialData() {
  let selectedCountry = document.getElementById("country-select").value;
  let selectedDate = document.getElementById("date-select").value;
  let selectedTime = document.getElementById("time-select").value;

  let initialData = {
    selectedCountry: selectedCountry,
    selectedDate: selectedDate,
    selectedTime: selectedTime,
  };

  return initialData;
}

confirmBtn1.addEventListener("click", () => {
  globalData.initialData = fetchInitialData();
  console.log(globalData);

  // Load inputs-2
  inputs1.style.display = "none";
  inputs2.style.display = "block";

  sendWebServiceRequestForAirports(
    globalData.initialData.selectedCountry,
    "getAirports"
  );
});

/**
 * getAirports Function
 * called by the airports api with an array of dometic airports objects when a get request is sent to it.
 * @param {{}[]} airportsJson Array of dometic airport objects.
 */
function getAirports(airportsJson) {
  globalData.domesticAirports = airportsJson;
  globalData.domesticAirportsToName();
  globalData.domesticAirportsToID();
  console.log(globalData.domesticAirportNames);
  globalData.renderAirportSelect();
}

confirmBtn2.addEventListener("click", () => {
  let selectedAirport = airportSelect.value;
  let selectedAirportIndex =
    globalData.domesticAirportNames.indexOf(selectedAirport);

  let startingAirport = globalData.domesticAirports[selectedAirportIndex];

  document.querySelector("form").style.display = "none";
  inputs2.style.display = "none";
  mapContainer.style.display = "flex";

  view.addTrip(startingAirport);
});


/**
 * getRoutes Function
 * called by the routes api with an array of outgoing route objects when a get request is sent to it.
 * @param {{}[]} routesJson Array of route objects
 */
function getRoutes(routesJson) {
  view.transeformRoutesToDestinations(routesJson);
}

/*-- button event handlers --*/

document.querySelector(".undo-btn").addEventListener('click', () => {
    if (view.stack.length > 1) {
        
        view.undo();
    }
});

document.querySelector(".trip-confirm-btn").addEventListener('click', () => {

  
  view.renderSummary();
});

document.querySelector(".trip-save-btn").addEventListener('click', () => {

  view.date = globalData.initialData.selectedDate;
  view.time = globalData.initialData.selectedTime;
  updateLSData(VIEWKEY, view);
  if (checkLSData(ALLTRIPS)) {
    all_trips = retrieveLSData(ALLTRIPS);
  }
  all_trips.push(view);
  
  updateLSData(ALLTRIPS, all_trips);

  window.location = 'trip-detailed-info.html'; 

});

document.querySelector(".trip-cancel-btn").addEventListener('click', () => {

  window.location = 'index-mdl.html';  
});


/**
 * renderMap Function
 * Fetches a new map from mapbox api and returns it
 * @returns {Map} A mapboxgl map object
 */
function renderMap() {
  document.querySelector(".map-container").innerHTML =
    "<div id='map' style='width: 1200px; height: 600px;'></div>";



  mapboxgl.accessToken =
    "pk.eyJ1Ijoic2hlaGFuMTIzIiwiYSI6ImNsMzM4aXA5ZzAxbzUzanFvZnZsMWJidWcifQ.xJmdtlOxpCnOyJ7noCUUNw";

  let map = new mapboxgl.Map({
    container: "map",
    style: "mapbox://styles/mapbox/streets-v11",
  });

  map.panTo();

  map.on("click", function (e) {
    console.log(e.lngLat);
    let { lng, lat } = e.lngLat;
    map.panTo([lng, lat]);
  });

  return map;
}
